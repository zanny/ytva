#! /usr/bin/env python3

"""
(C) Matt Scheirer 2015

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import subprocess
import sys
from os import path

mp4_extensions = ('m4v', 'mp4', 'lrv', 'f4v', 'flv')

def valid_extension(filename):
    if filename[-3:].lower() in mp4_extensions: 
        return True

parser = argparse.ArgumentParser(description='Convert youtube mp4 files to m4a audio files using ffmpeg')
parser.add_argument('video', help='mp4 video to convert')
parser.add_argument('-o', '--output', help='Custom destination for audio file - by default, the filename is preserved in the same location as the source video')
args = parser.parse_args()

# Input validation for the <video> arg
if not path.isfile(args.video):
    sys.exit('Video "{}" does not exist in filesystem or is not readable'.format(args.video))

if not valid_extension(args.video):
    sys.exit('Video needs to have one of these mp4 extensions: {}'.format(mp4_extensions))

if(args.output):
    # Input validation for the <output> arg
    if path.exists(args.output):
        sys.exit('Output file "{}" already exists in filesystem')
    if args.output[-3:] != 'm4a':
        sys.exit('Output file "{}" must end in an m4a file extension')
    directory = path.dirname(args.output)
    if directory and not isdir(directory):
        sys.exit('Output directory "{}" does not exist'.format(directory))
    output = args.output
else:
    output = args.video[:-4] + '.m4a'

subprocess.call(['ffmpeg', '-i', args.video, '-c:a', 'copy', '-vn', '-sn', output])